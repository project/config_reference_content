<?php

namespace Drupal\config_reference_content\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Plugin\views\filter\TaxonomyIndexTid;

/**
 * Filter by term id.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("taxonomy_index_uuid")
 */
class TaxonomyIndexUuid extends TaxonomyIndexTid {

  protected $uuids;

  public function buildExtraOptionsForm(&$form, FormStateInterface $form_state) {
    $vocabularies = $this->vocabularyStorage->loadMultiple();
    $options = [];
    foreach ($vocabularies as $voc) {
      $options[$voc->id()] = $voc->label();
    }

    if ($this->options['limit']) {
      // We only do this when the form is displayed.
      if (empty($this->options['vid'])) {
        $first_vocabulary = reset($vocabularies);
        $this->options['vid'] = $first_vocabulary->id();
      }

      if (empty($this->definition['vocabulary'])) {
        $form['vid'] = [
          '#type' => 'radios',
          '#title' => $this->t('Vocabulary'),
          '#options' => $options,
          '#description' => $this->t('Select which vocabulary to show terms for in the regular options.'),
          '#default_value' => $this->options['vid'],
        ];
      }
    }

    $form['type'] = [
      '#type' => 'value',
      '#value' => 'select',
      '#default_value' => $this->options['type'],
    ];

    $form['hierarchy'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show hierarchy in dropdown'),
      '#default_value' => !empty($this->options['hierarchy']),
    ];
  }

  public function validateExposed(&$form, FormStateInterface $form_state) {
    parent::validateExposed($form, $form_state);
  }

  public function buildExposeForm(&$form, FormStateInterface $form_state) {
    parent::buildExposeForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    $this->value = $this->switchUuidForId();
    $result = parent::adminSummary();
    $this->value = $this->switchIdForUuid();
    return $result;
  }

  protected function switchUuidForId() {
    $this->uuids = $this->value;
    $values = NULL;
    if (is_array($this->value)) {
      $values = [];
      foreach ($this->value as $uuid => $v) {
        $term = \Drupal::service('entity.repository')
          ->loadEntityByUuid('taxonomy_term', $uuid);
        if ($term === NULL) {
          continue;
        }
        $values[$term->id()] = $term->id();
      }
    }
    else {
      $term = \Drupal::service('entity.repository')
        ->loadEntityByUuid('taxonomy_term', $this->value);
      $values = $term->id();
    }
    return $values;
  }

  protected function switchIdForUuid() {
    return $this->uuids;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $this->options['value'] = $this->switchUuidForId();
    $dependencies = parent::calculateDependencies();
    $this->options['value'] = $this->switchIdForUuid();
    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->value = $this->switchUuidForId();
    parent::query();
    $this->value = $this->switchIdForUuid();
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $this->value = $this->switchUuidForId();
    $errors = parent::validate();
    $this->value = $this->switchIdForUuid();
    return $errors;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['type'] = ['default' => 'select'];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $vocabulary = $this->vocabularyStorage->load($this->options['vid']);
    if (empty($vocabulary) && $this->options['limit']) {
      $form['markup'] = [
        '#markup' => '<div class="js-form-item form-item">' . $this->t('An invalid vocabulary is selected. Please change it in the options.') . '</div>',
      ];
      return;
    }

    if (!empty($this->options['hierarchy']) && $this->options['limit']) {
      $tree = $this->termStorage->loadTree($vocabulary->id(), 0, NULL, TRUE);
      $options = [];

      if ($tree) {
        /** @var Term $term */
        foreach ($tree as $term) {
          if (!$term->isPublished() && !$this->currentUser->hasPermission('administer taxonomy')) {
            continue;
          }
          $choice = new \stdClass();
          $choice->option = [
            $term->uuid() => str_repeat('-', $term->depth) . \Drupal::service('entity.repository')
                ->getTranslationFromContext($term)
                ->label(),
          ];
          $options[] = $choice;
        }
      }
    }
    else {
      $options = [];
      $query = \Drupal::entityQuery('taxonomy_term')
        ->accessCheck(TRUE)
        // @todo Sorting on vocabulary properties -
        //   https://www.drupal.org/node/1821274.
        ->sort('weight')
        ->sort('name')
        ->addTag('taxonomy_term_access');
      if (!$this->currentUser->hasPermission('administer taxonomy')) {
        $query->condition('status', 1);
      }
      if ($this->options['limit']) {
        $query->condition('vid', $vocabulary->id());
      }
      $terms = Term::loadMultiple($query->execute());
      foreach ($terms as $term) {
        $options[$term->uuid()] = \Drupal::service('entity.repository')
          ->getTranslationFromContext($term)
          ->label();
      }
    }

    $default_value = (array) $this->value;

    if ($exposed = $form_state->get('exposed')) {
      $identifier = $this->options['expose']['identifier'];

      if (!empty($this->options['expose']['reduce'])) {
        $options = $this->reduceValueOptions($options);

        if (!empty($this->options['expose']['multiple']) && empty($this->options['expose']['required'])) {
          $default_value = [];
        }
      }

      if (empty($this->options['expose']['multiple'])) {
        if (empty($this->options['expose']['required']) && (empty($default_value) || !empty($this->options['expose']['reduce']))) {
          $default_value = 'All';
        }
        elseif (empty($default_value)) {
          $keys = array_keys($options);
          $default_value = array_shift($keys);
        }
        // Due to #1464174 there is a chance that array('') was saved in the admin ui.
        // Let's choose a safe default value.
        elseif ($default_value == ['']) {
          $default_value = 'All';
        }
        else {
          $copy = $default_value;
          $default_value = array_shift($copy);
        }
      }
    }
    $form['value'] = [
      '#type' => 'select',
      '#title' => $this->options['limit'] ? $this->t('Select terms from vocabulary @voc', ['@voc' => $vocabulary->label()]) : $this->t('Select terms'),
      '#multiple' => TRUE,
      '#options' => $options,
      '#size' => min(9, count($options)),
      '#default_value' => $default_value,
    ];

    $user_input = $form_state->getUserInput();
    if ($exposed && isset($identifier) && !isset($user_input[$identifier])) {
      $user_input[$identifier] = $default_value;
      $form_state->setUserInput($user_input);
    }

    if (!$form_state->get('exposed')) {
      // Retain the helper option
      $this->helper->buildOptionsForm($form, $form_state);

      // Show help text if not exposed to end users.
      $form['value']['#description'] = t('Leave blank for all. Otherwise, the first selected term will be the default instead of "Any".');
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function valueValidate($form, FormStateInterface $form_state) {
    parent::valueValidate($form, $form_state);
  }

}
