<?php

namespace Drupal\config_reference_content\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Config content generator plugins.
 */
abstract class ConfigContentGeneratorBase extends PluginBase implements ConfigContentGeneratorInterface {

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinition() {
    return $this->pluginDefinition;
  }

}
