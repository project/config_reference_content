<?php

namespace Drupal\config_reference_content\Plugin;

use Drupal\config_reference_content\Entity\ExportableContent;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigImporter;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Importer\MissingContentEvent;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the Config content generator plugin manager.
 */
class ConfigContentGeneratorManager extends DefaultPluginManager {

  const CONTENT_DEPENDENCY_KEY = 'content';

  const CONFIG_DEPENDENCY_KEY = 'config';

  const MODULE_DEPENDENCY_KEY = 'module';

  /**
   * Constructs a new ConfigContentGeneratorManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ConfigContentGenerator', $namespaces, $module_handler, 'Drupal\config_reference_content\Plugin\ConfigContentGeneratorInterface', 'Drupal\config_reference_content\Annotation\ConfigContentGenerator');

    $this->alterInfo('config_content_generator_info');
    $this->setCacheBackend($cache_backend, 'config_content_generator_plugins');
  }

  /**
   * Handles the update content event.
   *
   * @param array $context
   *   The batch context.
   *
   * @param \Drupal\Core\Config\ConfigImporter $config_importer
   *   The config importer.
   */
  public static function processUpdatedContent(array &$context, ConfigImporter $config_importer) {
    $processed_configurations = $config_importer->getProcessedConfiguration();
    foreach ($processed_configurations['update'] as $id) {
      $conf = explode('.', $id);
      if ($conf[0] === 'config_reference_content' && $conf[1] === 'exportable_content') {
        $exportable = ExportableContent::load($conf[2]);
        if (!$exportable instanceof ExportableContent) {
          continue;
        }
        $exportable->updateContentEntity();
      }
    }
    $context['finished'] = 1;
  }

  /**
   * Generate a content configuration if applicable.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityBase $entity
   *  The config.
   */
  public function addContentDependencies(ConfigEntityBase $entity) {
    $dependencies = $entity->getDependencies();
    foreach ($this->getDefinitions() as $id => $definition) {
      /** @var \Drupal\config_reference_content\Plugin\ConfigContentGeneratorInterface $instance */
      $instance = $this->createInstance($id);
      if (!$instance->isApplicable($entity)) {
        continue;
      }
      $contents = $instance->getContents($entity);
      if (empty($contents)) {
        continue;
      }
      foreach ($contents as $content) {
        if (!$content instanceof ContentEntityBase) {
          continue;
        }
        $content_dependency = $content->getConfigDependencyName();
        $dependencies[static::CONTENT_DEPENDENCY_KEY][] = $content_dependency;
      }
    }
    $entity->set('dependencies', $dependencies);
  }

  public function generateExportableContent(ConfigEntityBase $entity) {
    $dependencies = $entity->getDependencies();
    if (empty($dependencies[static::CONTENT_DEPENDENCY_KEY])) {
      return;
    }
    foreach ($dependencies[static::CONTENT_DEPENDENCY_KEY] as $dependency) {
      $config = ExportableContent::loadByDependency($dependency);
      if ($config === NULL) {
        $config = ExportableContent::createByDependency($dependency);
        $config->save();
      }
    }
  }

  public function updateExportableContent(ContentEntityBase $entity) {
    $content_dependency = $entity->getConfigDependencyName();
    $config = ExportableContent::loadByDependency($content_dependency);
    if (!$config instanceof ExportableContent) {
      return;
    }
    $content = ExportableContent::encodeContent($entity);
    $config->set('content', $content);
    $config->save();
  }

  /**
   * Handles the missing content event.
   *
   * @param \Drupal\Core\Config\Importer\MissingContentEvent $event
   *   The missing content event.
   */
  public function processMissingContent(MissingContentEvent $event) {
    $items = [];
    foreach ($event->getMissingContent() as $uuid => $missing) {
      $dependency = implode(':', $missing);
      $exportable = ExportableContent::loadByDependency($dependency);
      if ($exportable === NULL) {
        continue;
      }
      $dependencies = $exportable->getDependencies();
      $deps = [];
      foreach ($dependencies['content'] as $content_dependency) {
        if ($content_dependency === $exportable->getContentDependencyId()) {
          continue;
        }
        $deps[] = $content_dependency;
      }
      $items[] = ['id' => $dependency, 'deps' => $deps];
    }
    $sorted = $this->sortDependencies($items);
    foreach ($sorted as $item) {
      $exportable = ExportableContent::loadByDependency($item['id']);
      if ($exportable !== NULL && $exportable->loadLocalContent() === NULL) {
        $exportable->createContentEntity();
      }

    }
  }

  /**
   * Sort all missing content by dependencies.
   *
   * @param array $items
   *   The array of missing contents and dependencies.
   * @param array $res
   *   The set of missing contents and their ordered dependencies.
   * @param string $currentDep
   *   Current dependency (recursion).
   *
   * @return array
   *   Sorted dependencies.
   */
  protected function sortDependencies(array $items, array &$res = [], $currentDep = NULL) {

    $itemsSend = $items;
    foreach ($items as $itemIndex => $item) {

      foreach ($item['deps'] as $dep) {

        if ($currentDep != $item['id']) {

          unset($itemsSend[$itemIndex]);
          $this->sortDependencies($itemsSend, $res, $dep);
        }
        else {
          $res[] = $item;
          return $res;
        }

      }
      if (!in_array($item, $res)) {
        $res[] = $item;
      }
    }
    return $res;
  }

}
