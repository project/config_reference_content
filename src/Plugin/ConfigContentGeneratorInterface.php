<?php

namespace Drupal\config_reference_content\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines an interface for Config content generator plugins.
 */
interface ConfigContentGeneratorInterface extends PluginInspectionInterface {

  /**
   * Returns if the plugin is applicable for this entity.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityBase $entity
   *  The config.
   *
   * @return bool
   *   TRUE if the widget can be used, FALSE otherwise.
   */
  public function isApplicable(ConfigEntityBase $entity);

  /**
   * Extract the contents inside the configuration.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityBase $entity
   *  The config.
   *
   * @return array
   *   An array of content entities.
   */
  public function getContents(ConfigEntityBase $entity);

}
