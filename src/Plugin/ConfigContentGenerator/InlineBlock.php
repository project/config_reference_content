<?php

namespace Drupal\config_reference_content\Plugin\ConfigContentGenerator;

use Drupal\block_content\Entity\BlockContent;
use Drupal\config_reference_content\Plugin\ConfigContentGeneratorBase;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;


/**
 * @ConfigContentGenerator(
 *  id = "inline_block",
 *  label = @Translation("Inline block"),
 * )
 */
class InlineBlock extends ConfigContentGeneratorBase {

  /**
   * {@inheritdoc}
   */
  public function isApplicable(ConfigEntityBase $entity) {
    return $entity instanceof LayoutBuilderEntityViewDisplay;
  }

  /**
   * {@inheritdoc}
   */
  public function getContents(ConfigEntityBase $entity) {
    $contents = [];
    $sections = $entity->getThirdPartySetting('layout_builder', 'sections');
    if (empty($sections)) {
      return $contents;
    }
    /** @var \Drupal\layout_builder\Section $section */
    foreach ($sections as $section) {
      /** @var \Drupal\layout_builder\SectionComponent $component */
      foreach ($section->getComponents() as $component) {
        $config = $component->get('configuration');
        if (strpos($config['id'], 'inline_block:') === 0) {
          $block = NULL;
          if (!empty($config['uuid'])) {
            $block = \Drupal::service('entity.repository')
              ->loadEntityByUuid('block_content', $config['uuid']);
          }
          elseif ($config['block_revision_id']) {
            $block = \Drupal::entityTypeManager()
              ->getStorage('block_content')
              ->loadRevision($config['block_revision_id']);
          }

          if (!$block instanceof BlockContent) {
            continue;
          }
          $contents[] = $block;
        }
      }
    }
    return $contents;
  }

}
