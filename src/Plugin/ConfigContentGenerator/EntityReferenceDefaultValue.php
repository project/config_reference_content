<?php

namespace Drupal\config_reference_content\Plugin\ConfigContentGenerator;

use Drupal\config_reference_content\Plugin\ConfigContentGeneratorBase;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\field\Entity\FieldConfig;


/**
 * @ConfigContentGenerator(
 *  id = "entity_reference_default_value",
 *  label = @Translation("Entity Reference Default Value"),
 * )
 */
class EntityReferenceDefaultValue extends ConfigContentGeneratorBase {

  /**
   * {@inheritdoc}
   */
  public function isApplicable(ConfigEntityBase $entity) {
    if ($entity instanceof FieldConfig) {
      $type = $entity->getType();
      $types = $this->getApplicableTypes();
      if (!empty($types[$type])) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Get an array of entity reference types.
   *
   * @return array
   *   An array of entity_reference types.
   */
  protected function getApplicableTypes() {
    return [
      'entity_reference' => 'target_uuid',
      'image' => 'default_image',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getContents(ConfigEntityBase $entity) {
    $contents = [];
    $default_values = $entity->getDefaultValueLiteral();
    $type = $entity->getType();
    $types = $this->getApplicableTypes();
    $key = $types[$type];
    $target_type = $entity->getFieldStorageDefinition()
      ->getSetting('target_type');
    if (empty($default_values)) {
      $settings = $entity->getSettings();
      if (!empty($settings[$key]['uuid'])) {
        $default_values[] = [$key => $settings[$key]['uuid']];
      }
    }
    foreach ($default_values as $default) {
      if (empty($default[$key])) {
        continue;
      }
      $contents[] = \Drupal::service('entity.repository')
        ->loadEntityByUuid($target_type, $default[$key]);
    }
    return $contents;
  }

}
