<?php

namespace Drupal\config_reference_content;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Exportable content entities.
 */
class ExportableContentListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    //$header['label'] = $this->t('Exportable content');
    $header['target_entity_type'] = $this->t('Target Entity Type');
    $header['target_bundle'] = $this->t('Target Bundle');
    $header['target_uuid'] = $this->t('Target UUID');
    $header['id'] = $this->t('Machine name');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    //$row['label'] = $entity->label();
    $row['target_entity_type'] = $entity->get('target_entity_type');
    $row['target_bundle'] = $entity->get('target_bundle');
    $row['target_uuid'] = $entity->get('target_uuid');
    $row['id'] = $entity->id();
    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations(EntityInterface $entity) {
    $operations = parent::getOperations($entity);
    $content = $entity->decodeContent();
    $links = \Drupal::entityTypeManager()
      ->getDefinition($content->getEntityTypeId())
      ->getLinkTemplates();
    if (empty($links['edit-form'])) {
      return $operations;
    }
    $operations['content'] = [
      'title' => $this->t('View Content'),
      'weight' => 100,
      'url' => $this->ensureDestination($content->toUrl('edit-form')),
    ];
    return $operations;
  }

}
