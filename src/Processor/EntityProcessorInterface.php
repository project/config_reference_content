<?php

namespace Drupal\config_reference_content\Processor;


use Drupal\Core\Entity\ContentEntityBase;

interface EntityProcessorInterface {

  /**
   * Encode the entity for saving as config.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   The content entity object.
   *
   * @return array
   *   The entity transformed to array.
   */
  public function encode(ContentEntityBase $entity);

  /**
   * Decode a raw data and convert to entity object.
   *
   * @param array $data
   *   The entity raw data.
   * @param string $entity_type
   *   The entity type.
   *
   * @return ContentEntityBase
   *   The entity object decoded.
   */
  public function decode(array $data, string $entity_type);

  /**
   * Calculate de content dependencies.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   The content entity object.
   * @param array $dependencies
   *   The Exportable Content entity dependencies.
   */
  public function calculateDependencies(ContentEntityBase $entity, array &$dependencies);

}
