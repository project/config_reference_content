<?php

namespace Drupal\config_reference_content\Processor;

use Drupal\Core\Entity\ContentEntityBase;

class FileEntityProcessor extends DefaultEntityProcessor {

  /**
   * {@inheritdoc}
   */
  public function encode(ContentEntityBase $entity) {
    $data = parent::encode($entity);
    $file = file_get_contents($entity->getFileUri());
    $data['file__encoded'] = base64_encode($file);
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function decode(array $data, string $entity_type) {
    $file = base64_decode($data['file__encoded']);
    unset($data['file__encoded']);
    $entity = parent::decode($data, $entity_type);
    if (!file_exists($entity->getFileUri())) {
      file_put_contents($entity->getFileUri(), $file);
    }
    return $entity;
  }

}
