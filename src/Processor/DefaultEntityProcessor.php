<?php

namespace Drupal\config_reference_content\Processor;

use Drupal\config_reference_content\Plugin\ConfigContentGeneratorManager;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\EntityReferenceFieldItemList;

class DefaultEntityProcessor implements EntityProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function encode(ContentEntityBase $entity) {
    $settings = $this->getConfiguration();
    $entity_type_id = $entity->getEntityTypeId();
    $bundle = $entity->bundle();
    $values = [];
    foreach ($entity->getFields() as $name => $field) {
      $values[$name] = $field->getValue();
      if ($field instanceof EntityReferenceFieldItemList) {
        foreach ($field as $key => $item) {
          if ($item->entity instanceof ContentEntityBase && !empty($settings[$entity_type_id][$bundle][$name])) {
            $values[$name][$key]['target_uuid'] = $item->entity->get('uuid')->value;
            unset($values[$name][$key]['target_id']);
          }
          elseif ($item->entity instanceof ContentEntityBase) {
            unset($values[$name]);
          }
        }
      }
    }
    return $values;
  }

  protected function getConfiguration() {
    return \Drupal::configFactory()
      ->get('config_reference_content.exportable_content_settings')
      ->get('settings');
  }

  /**
   * {@inheritdoc}
   */
  public function decode(array $data, string $entity_type) {
    $settings = $this->getConfiguration();
    /** @var \Drupal\Core\Entity\EntityTypeManager $entity_type_manager */
    $entity_type_manager = \Drupal::service('entity_type.manager');
    /** @var \Drupal\Core\Entity\ContentEntityBase $entity */
    $entity = $entity_type_manager->getStorage($entity_type)->create($data);
    $entity_type_id = $entity->getEntityTypeId();
    $bundle = $entity->bundle();
    foreach ($data as $field => $values) {
      if ($entity->get($field) instanceof EntityReferenceFieldItemList && !empty($settings[$entity_type_id][$bundle][$field])) {
        foreach ($values as $key => $value) {
          if (empty($value['target_uuid'])) {
            continue;
          }
          $content = \Drupal::service('entity.repository')
            ->loadEntityByUuid($entity_type_id, $value['target_uuid']);
          if (!empty($content)) {
            $values[$key]['target_id'] = $content->id();
          }
        }
        $entity->set($field, $values);
      }
    }
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(ContentEntityBase $entity, array &$dependencies) {
    $settings = $this->getConfiguration();
    $entity_type_id = $entity->getEntityTypeId();
    $bundle = $entity->bundle();
    $content_dependencies = $dependencies[ConfigContentGeneratorManager::CONTENT_DEPENDENCY_KEY];
    foreach ($entity->getFields() as $name => $field) {
      if (!$field instanceof EntityReferenceFieldItemList || empty($settings[$entity_type_id][$bundle][$name])) {
        continue;
      }
      foreach ($field as $item) {
        if ($item->entity instanceof ContentEntityBase) {
          $content_dependencies[$item->entity->getConfigDependencyName()] = $item->entity->getConfigDependencyName();
        }
        elseif (!empty($item->getValue())) {
          $value = $item->getValue();
          $settings = $item->getFieldDefinition()->getSettings();
          if (!empty($value['target_uuid'])) {
            $content = \Drupal::service('entity.repository')
              ->loadEntityByUuid($settings['target_type'], $value['target_uuid']);
            if ($content instanceof ContentEntityBase) {
              $content_dependencies[$content->getConfigDependencyName()] = $content->getConfigDependencyName();
            }
          }
        }
      }
    }
    $dependencies[ConfigContentGeneratorManager::CONTENT_DEPENDENCY_KEY] = array_values($content_dependencies);
  }

}
