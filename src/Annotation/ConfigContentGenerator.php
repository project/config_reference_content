<?php

namespace Drupal\config_reference_content\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Config content generator item annotation object.
 *
 * @see \Drupal\config_reference_content\Plugin\ConfigContentGeneratorManager
 * @see plugin_api
 *
 * @Annotation
 */
class ConfigContentGenerator extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
