<?php

namespace Drupal\config_reference_content\Form;

use Drupal\Component\Serialization\Yaml;
use Drupal\config_reference_content\Entity\ExportableContent;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ExportableContentForm.
 */
class ExportableContentForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $exportable_content = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $exportable_content->label(),
      '#description' => $this->t("Label for the Exportable content."),
      '#required' => TRUE,
      '#access' => FALSE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $exportable_content->id(),
      '#machine_name' => [
        'exists' => '\Drupal\config_reference_content\Entity\ExportableContent::load',
      ],
      '#disabled' => !$exportable_content->isNew(),
      '#access' => FALSE,
    ];

    $options = [];
    foreach ($this->entityTypeManager->getDefinitions() as $id => $def) {
      if ($def instanceof ContentEntityType) {
        $options[$id] = $def->getLabel();
      }
    }

    $target_entity_type = $exportable_content->get('target_entity_type');
    $target_uuid = $exportable_content->get('target_uuid');
    $form['target_entity_type'] = [
      '#title' => $this->t('Entity type'),
      '#type' => 'radios',
      '#options' => $options,
      '#default_value' => $target_entity_type,
      '#disabled' => !$exportable_content->isNew(),
    ];

    $form['target_uuid'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];
    foreach ($options as $entity_type_id => $label) {
      $default_value = NULL;
      if ($entity_type_id === $target_entity_type && !empty($target_uuid)) {
        $entities = $this->entityTypeManager->getStorage($target_entity_type)
          ->loadByProperties(['uuid' => $target_uuid]);
        $default_value = reset($entities);
      }
      $form['target_uuid'][$entity_type_id] = [
        '#title' => $this->t('Entity'),
        '#type' => 'entity_autocomplete',
        '#target_type' => $entity_type_id,
        '#validate_reference' => FALSE,
        '#default_value' => $default_value,
        '#disabled' => !empty($target_uuid),
        '#states' => [
          'visible' => [
            '[name="target_entity_type"]' => ['value' => $entity_type_id],
          ],
        ],
      ];
    }

    if (!$exportable_content->isNew()) {
      $form['content'] = [
        '#type' => 'textarea',
        '#title' => t('Content'),
        '#attributes' => ['data-yaml-editor' => 'true'],
        '#rows' => 20,
        '#disabled' => TRUE,
        '#default_value' => Yaml::encode($exportable_content->get('content')),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $exportable_content = $this->entity;
    $values = $form_state->getValues();
    $target_id = $values['target_uuid'][$values['target_entity_type']];
    $entity = $this->entityTypeManager->getStorage($values['target_entity_type'])
      ->load($target_id);
    $exportable_content->set('target_bundle', $entity->bundle());
    $exportable_content->set('target_uuid', $entity->get('uuid')->value);
    $content = ExportableContent::encodeContent($entity);
    $exportable_content->set('content', $content);
    $exportable_content->set('generator', 'config_reference_content');
    if ($exportable_content->isNew()) {
      $exportable_content->set('id', ExportableContent::getIdByDependency($entity->getConfigDependencyName()));
      $exportable_content->set('label', $entity->label());
    }
    $status = $exportable_content->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addMessage($this->t('Created the %label Exportable content.', [
            '%label' => $exportable_content->label(),
          ]));
        break;

      default:
        $this->messenger()
          ->addMessage($this->t('Saved the %label Exportable content.', [
            '%label' => $exportable_content->label(),
          ]));
    }
    $form_state->setRedirectUrl($exportable_content->toUrl('collection'));
  }

}
