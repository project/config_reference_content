<?php

namespace Drupal\config_reference_content\Form;

use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class exportable_content_settings.
 */
class ExportableContentSettings extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * \Drupal\Core\Entity\EntityTypeBundleInfoInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityTypeBundleInfo = $container->get('entity_type.bundle.info');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'exportable_content_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('config_reference_content.exportable_content_settings');

    $entity_types = $this->entityTypeManager->getDefinitions();
    $labels = [];
    $default = [];

    $bundles = $this->entityTypeBundleInfo->getAllBundleInfo();
    foreach ($entity_types as $entity_type_id => $entity_type) {
      if (!$entity_type instanceof ContentEntityTypeInterface) {
        continue;
      }
      $labels[$entity_type_id] = $entity_type->getLabel() ?: $entity_type_id;
    }

    $form['disable_automatic_generation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable automatic generation'),
      '#description' => $this->t('Check if you dont want to generate automatically exportable content when the module detect content dependencies.'),
      '#default_value' => empty($config->get('disable_automatic_generation')) ? FALSE : TRUE,
    ];

    asort($labels);
    $form['entity_types'] = [
      '#title' => $this->t('Entity types'),
      '#type' => 'checkboxes',
      '#options' => $labels,
      '#default_value' => $config->get('entity_types'),
    ];

    $form['settings'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];
    $settings = $config->get('settings');
    foreach ($labels as $entity_type_id => $label) {
      $entity_type = $entity_types[$entity_type_id];

      $form['settings'][$entity_type_id] = [
        '#title' => $label,
        '#type' => 'details',
        '#open' => FALSE,
        '#states' => [
          'visible' => [
            ':input[name="entity_types[' . $entity_type_id . ']"]' => ['checked' => TRUE],
          ],
        ],
      ];
      foreach ($bundles[$entity_type_id] as $bundle => $bundle_info) {
        $ref_options = $this->getEntityReferenceFields($entity_type_id, $bundle);
        $form['settings'][$entity_type_id][$bundle] = [
          '#title' => $bundle_info['label'],
          '#type' => 'checkboxes',
          '#options' => $ref_options,
          '#default_value' => !empty($settings[$entity_type_id][$bundle]) ? $settings[$entity_type_id][$bundle] : [],
          '#access' => !empty($ref_options),
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  protected function getEntityReferenceFields($entity_type_id, $bundle) {
    $result = [];
    $er_types = [
      'entity_reference',
      'image',
      'file',
      'entity_reference_revisions',
    ];
    foreach ($this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle) as $field_name => $field) {
      if (in_array($field->getType(), $er_types, TRUE) && $field_name !== 'revision_uid' && $field_name !== 'revision_user') {
        $target_type = $field->getItemDefinition()->getSetting('target_type');
        if (!empty($target_type) && $this->entityTypeManager->getDefinition($target_type) instanceof ContentEntityType) {
          $result[$field_name] = $field->getLabel();
        }
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();

    $this->config('config_reference_content.exportable_content_settings')
      ->set('disable_automatic_generation', $values['disable_automatic_generation'])
      ->set('entity_types', array_filter($values['entity_types']))
      ->set('settings', array_filter($values['settings']))
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'config_reference_content.exportable_content_settings',
    ];
  }

}
