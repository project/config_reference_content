<?php

namespace Drupal\config_reference_content\EventSubscriber;

use Drupal\config_reference_content\Plugin\ConfigContentGeneratorManager;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\Importer\MissingContentEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Final event subscriber to the missing content event.
 *
 * Ensure that all missing content dependencies are removed from the event so
 * the importer can complete.
 *
 * @see \Drupal\Core\Config\ConfigImporter::processMissingContent()
 */
class MissingContentSubscriber implements EventSubscriberInterface {

  /**
   * The generator manager.
   *
   * @var \Drupal\config_reference_content\Plugin\ConfigContentGeneratorManager
   */
  protected $generatorManager;

  public function __construct(ConfigContentGeneratorManager $generator_manager) {
    $this->generatorManager = $generator_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // This should always be the final event as it will mark all content
    // dependencies as resolved.
    $events[ConfigEvents::IMPORT_MISSING_CONTENT][] = [
      'onMissingContent',
      0,
    ];
    return $events;
  }

  /**
   * Handles the missing content event.
   *
   * @param \Drupal\Core\Config\Importer\MissingContentEvent $event
   *   The missing content event.
   */
  public function onMissingContent(MissingContentEvent $event) {
    $this->generatorManager->processMissingContent($event);
  }

}
