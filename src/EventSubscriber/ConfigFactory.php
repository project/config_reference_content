<?php

namespace Drupal\config_reference_content\EventSubscriber;

use Drupal\config_reference_content\Plugin\ConfigContentGeneratorManager;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Defines the configuration object factory.
 *
 * The configuration object factory instantiates a Config object for each
 * configuration object name that is accessed and returns it to callers.
 *
 * @see \Drupal\Core\Config\Config
 *
 * Each configuration object gets a storage object injected, which
 * is used for reading and writing the configuration data.
 *
 * @see \Drupal\Core\Config\StorageInterface
 *
 * @ingroup config_api
 */
class ConfigFactory implements EventSubscriberInterface {

  /**
   * The generator manager.
   *
   * @var \Drupal\config_reference_content\Plugin\ConfigContentGeneratorManager
   */
  protected $generatorManager;

  /**
   * Constructs the Config factory.
   *
   * @param \Drupal\config_reference_content\Plugin\ConfigContentGeneratorManager $generator_manager
   *   The generator manager.
   */
  public function __construct(ConfigContentGeneratorManager $generator_manager) {
    $this->generatorManager = $generator_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::SAVE][] = ['onConfigSaveAddDependencies', 0];
    $events[ConfigEvents::SAVE][] = ['onConfigSave', -1];
    return $events;
  }

  /**
   * Updates stale static cache entries when configuration is saved.
   *
   * @param ConfigCrudEvent $event
   *   The configuration event.
   */
  public function onConfigSaveAddDependencies(ConfigCrudEvent $event) {
    $saved_config = $event->getConfig();
    //$this->generatorManager->addContentDependencies($saved_config);
  }

  public function onConfigSave(ConfigCrudEvent $event) {
    $saved_config = $event->getConfig();
    //$this->generatorManager->generate($saved_config);
  }

}
