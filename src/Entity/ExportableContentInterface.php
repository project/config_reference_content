<?php

namespace Drupal\config_reference_content\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Exportable content entities.
 */
interface ExportableContentInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
