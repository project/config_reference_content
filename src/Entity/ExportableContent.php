<?php

namespace Drupal\config_reference_content\Entity;

use Drupal\config_reference_content\Plugin\ConfigContentGeneratorManager;
use Drupal\config_reference_content\Processor\EntityProcessorInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\ContentEntityBase;

/**
 * Defines the Exportable content entity.
 *
 * @ConfigEntityType(
 *   id = "exportable_content",
 *   label = @Translation("Exportable content"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" =
 *   "Drupal\config_reference_content\ExportableContentListBuilder",
 *     "form" = {
 *       "add" = "Drupal\config_reference_content\Form\ExportableContentForm",
 *       "edit" = "Drupal\config_reference_content\Form\ExportableContentForm",
 *       "delete" =
 *   "Drupal\config_reference_content\Form\ExportableContentDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" =
 *   "Drupal\config_reference_content\ExportableContentHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "exportable_content",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" =
 *   "/admin/structure/exportable_content/{exportable_content}",
 *     "add-form" = "/admin/structure/exportable_content/add",
 *     "edit-form" =
 *   "/admin/structure/exportable_content/{exportable_content}/edit",
 *     "delete-form" =
 *   "/admin/structure/exportable_content/{exportable_content}/delete",
 *     "collection" = "/admin/structure/exportable_content"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "target_entity_type",
 *     "target_bundle",
 *     "target_uuid",
 *     "generator",
 *     "content",
 *     "settings"
 *   }
 * )
 */
class ExportableContent extends ConfigEntityBase implements ExportableContentInterface {

  /**
   * The Exportable content ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Exportable content label.
   *
   * @var string
   */
  protected $label;

  /**
   * The content entity type id.
   *
   * @var string
   */
  protected $target_entity_type;

  /**
   * The content bundle id.
   *
   * @var string
   */
  protected $target_bundle;

  /**
   * The target content uuid.
   *
   * @var string
   */
  protected $target_uuid;

  /**
   * The generator plugin id.
   *
   * @var string
   */
  protected $generator;

  /**
   * The content entity as array.
   *
   * @var array
   */
  protected $content;

  /**
   * The synchronizable strategy settings.
   *
   * @var array
   */
  protected $settings;

  /**
   * Create a exportable entity by dependency id.
   *
   * @param string $content_dependency
   *
   * @return \Drupal\config_reference_content\Entity\ExportableContent
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function createByDependency(string $content_dependency) {
    $entity_type_manager = \Drupal::entityTypeManager();
    [$entity_type, $bundle, $uuid] = explode(':', $content_dependency);
    $contents = $entity_type_manager->getStorage($entity_type)
      ->loadByProperties(['uuid' => $uuid]);
    /** @var \Drupal\Core\Entity\ContentEntityBase $content */
    $content = reset($contents);
    $data = static::encodeContent($content);
    $id = static::getIdByDependency($content_dependency);
    $entity = [
      'id' => $id,
      'label' => $content->label(),
      'target_entity_type' => $entity_type,
      'target_bundle' => $bundle,
      'target_uuid' => $uuid,
      'content' => $data,
    ];
    return static::create($entity);
  }

  public static function encodeContent(ContentEntityBase $entity) {
    $class = $entity->getEntityType()
      ->get('config_reference_content_processor');
    $processor = \Drupal::service('class_resolver')
      ->getInstanceFromDefinition($class);
    if (!$processor instanceof EntityProcessorInterface) {
      return NULL;
    }
    return $processor->encode($entity);
  }

  public static function getIdByDependency(string $content_dependency) {
    return str_replace([':', '-'], '_', $content_dependency);
  }

  /**
   * @param string $content_dependency
   *
   * @return \Drupal\config_reference_content\Entity\ExportableContent|null
   */
  public static function loadByDependency(string $content_dependency) {
    $id = static::getIdByDependency($content_dependency);
    return static::load($id);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();
    $dependencies = $this->getDependencies();
    /** @var \Drupal\Core\Entity\ContentEntityBase $content */
    $content = $this->entityTypeManager()
      ->getStorage($this->target_entity_type)
      ->create($this->content);
    $dependencies[ConfigContentGeneratorManager::CONTENT_DEPENDENCY_KEY][] = $content->getConfigDependencyName();
    $this->getProcessor()->calculateDependencies($content, $dependencies);
    $this->addDependencies($dependencies);
  }

  /**
   * Get the content entity processor class.
   *
   * @return \Drupal\config_reference_content\Processor\EntityProcessorInterface
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getProcessor() {
    $class = $this->entityTypeManager()
      ->getDefinition($this->target_entity_type)
      ->get('config_reference_content_processor');
    $processor = \Drupal::service('class_resolver')
      ->getInstanceFromDefinition($class);
    if (!$processor instanceof EntityProcessorInterface) {
      return NULL;
    }
    return $processor;
  }

  public function updateContentEntity() {
    /** @var \Drupal\Core\Entity\ContentEntityBase $content */
    $content = $this->loadLocalContent();
    $entity = $this->decodeContent();
    $entity->setSyncing(TRUE);
    $entity->enforceIsNew(FALSE);
    $keys = $this->entityTypeManager()
      ->getDefinition($entity->getEntityTypeId())
      ->getKeys();
    $entity->set($keys['id'], $content->id());
    $entity->set($keys['revision'], $content->getRevisionId());
    if ($entity->hasField('revision_id')) {
      $entity->set('revision_id', NULL);
    }
    if ($entity->hasField('revision_created')) {
      $entity->set('revision_created', NULL);
    }
    if ($entity->hasField('revision_user')) {
      $entity->set('revision_user', NULL);
    }
    if ($entity->hasField('revision_log')) {
      $entity->set('revision_log', NULL);
    }
    $entity->save();
  }

  public function loadLocalContent() {
    $entities = $this->entityTypeManager()
      ->getStorage($this->target_entity_type)
      ->loadByProperties(['uuid' => $this->target_uuid]);
    if (empty($entities)) {
      return NULL;
    }
    return reset($entities);
  }

  public function decodeContent() {
    return $this->getProcessor()
      ->decode($this->content, $this->target_entity_type);
  }

  public function createContentEntity() {
    $entity = $this->decodeContent();
    $entity->setSyncing(TRUE);
    $entity->enforceIsNew(TRUE);
    $keys = $this->entityTypeManager()
      ->getDefinition($entity->getEntityTypeId())
      ->getKeys();
    if (!empty($keys['id'])) {
      $entity->set($keys['id'], NULL);
    }
    if (!empty($keys['revision'])) {
      $entity->set($keys['revision'], NULL);
    }
    if ($entity->hasField('revision_id')) {
      $entity->set('revision_id', NULL);
    }
    if ($entity->hasField('revision_created')) {
      $entity->set('revision_created', NULL);
    }
    if ($entity->hasField('revision_user')) {
      $entity->set('revision_user', NULL);
    }
    if ($entity->hasField('revision_log')) {
      $entity->set('revision_log', NULL);
    }

    $entity->save();
  }

  public function getContentDependencyId() {
    return $this->target_entity_type . ':' . $this->target_bundle . ':' . $this->target_uuid;
  }

}
